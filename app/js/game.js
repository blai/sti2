angular.module('sti2')
  .factory('GameEngine', function($q, $document, roller) {
    // random integer between min (included) and max (excluded)
    function getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }

    function fillArray(n) {
      var ret = [];
      for (var i = 1; i <= n; i++) {
        ret.push(i);
      }
      return ret;
    }

    function GameEngine(diceCount, facePerDie, maxRoll) {
      this.diceCount = diceCount = Math.max(1, diceCount || 1);
      this.facePerDie = facePerDie = Math.max(4, facePerDie || 4);
      this.maxRoll = maxRoll = Math.max(1, maxRoll || 1);

      this.permutationsPerDice = fillArray(facePerDie);
      this.permutationsPerRoll = fillArray(facePerDie * diceCount);
      this.permutations = (function getPermutations(curDice) {
        var self = this;
        if (curDice === diceCount) {
          return self.permutationsPerDice.slice();
        }

        var exceptMe = getPermutations.call(self, ++curDice);
        return exceptMe.map(function(v) {
          return self.permutationsPerDice.map(function(p) {
            return p + v;
          });
        }).reduce(function(all, chunk) {
          return all.concat(chunk);
        }, []);
      }).call(this, 1);

      this.permutationCountCache = {};
    }

    GameEngine.prototype = {
      genTarget: function() {
        var min = this.diceCount * this.facePerDie; // maximum outcome of a single roll
        var max = min * this.maxRoll + 1;
        return getRandomInt(min, max);
      },

      rollDice: function(animation) {
        if (animation) {
          var deferred = $q.defer();

          function onRollerResult(event) {
            document.removeEventListener('roller:results', event);
            var total = event.values.reduce(function(total, p) {
              return total + p;
            }, 0);
            deferred.resolve(total);
          }

          function onRollerStopped(event) {
            document.removeEventListener('roller:results', onRollerResult);
            document.removeEventListener('roller:stopped', onRollerStopped);
            deferred.reject('stopped');
          }

          document.addEventListener('roller:results', onRollerResult);
          document.addEventListener('roller:stopped', onRollerStopped);
          roller.roll();
          return deferred.promise;
        } else {
          return getRandomInt(this.diceCount, this.diceCount * this.facePerDie + 1);
        }
      },

      chanceOfBusting: function(curPoints, target) {
        var pointsFromTarget = target - curPoints;
        var maxPointPerRoll = this.diceCount * this.facePerDie;
        var delta = pointsFromTarget - maxPointPerRoll;
        if (delta >= 0) {
          return 0; // no chance of busting
        }

        var self = this;
        var bustingPopulation = this.permutationsPerRoll.slice(pointsFromTarget).reduce(function(count, curPoint) {
          return count + self.getPopulation(curPoint);
        }, 0);

        return bustingPopulation / this.permutations.length;
      },

      getPopulation: function(points) {
        if (!this.permutationCountCache.hasOwnProperty(points)) {
          this.permutationCountCache[points] = this.permutations.reduce(function(count, cur) {
            if (cur === points) {
              count++;
            }
            return count;
          }, 0);
        }
        return this.permutationCountCache[points];
      }
    };

    return GameEngine;
  })
  .factory('Player', function() {
    var ids = 0;

    function Player(name) {
      this.id = ++ids;
      this.name = name || this.id;
      this.records = {
        win: 0,
        lose: 0,
        tie: 0
      }
    }

    Player.prototype = {
      logGame: function(status) {
        var key = status === 1 ? 'win' : status === 0 ? 'lose' : 'tie';
        this.records[key]++;
      }
    };

    return Player;
  })
  .factory('Game', function(Player) {
    function getTotalPoints(rolls, index) {
      return rolls.reduce(function(total, cur) {
        return total + (cur[index] || 0);
      }, 0);
    }

    function Game(engine, host, player) {
      this.engine = engine;
      this.host = host;
      this.player = player;
      this.rolls = [];
      this.target = this.engine.genTarget();
      this.result = null;
      this.running = false;
    }

    Game.prototype = {
      playerTotal: function() {
        return getTotalPoints(this.rolls, 1);
      },

      hostTotal: function() {
        return getTotalPoints(this.rolls, 0);
      },

      next: function(hit) {
        var self = this;
        var hostPoints = null;
        var hostChanceOfBusting;

        function done() {
          if (hostPoints === null && !hit) {
            self.rolls.push([hostPoints, null]);
            return self.determineResult(true);
          } else {
            return self.determineResult(false);
          }
        }

        if (hit) {
          this.running = true;
          this.engine.rollDice(true).then(function(playerPoints) {
            self.running = false;
            if ((self.playerTotal() + playerPoints) <= self.target) {
              hostChanceOfBusting = self.engine.chanceOfBusting(self.hostTotal(), self.target);
              if (hostChanceOfBusting <= 0.5 || (hostChanceOfBusting < 1 && self.hostTotal() < self.playerTotal())) {
                hostPoints = self.engine.rollDice();
              }
            }
            self.rolls.push([hostPoints, playerPoints]);
            done();
          });
        } else {
          var playerTotal = this.playerTotal();
          for (hostChanceOfBusting = this.engine.chanceOfBusting(this.hostTotal(), this.target); hostChanceOfBusting < 0.5 || (hostChanceOfBusting < 1 && this.hostTotal() < playerTotal); hostChanceOfBusting = this.engine.chanceOfBusting(this.hostTotal(), this.target)) {
            hostPoints = this.engine.rollDice();
            this.rolls.push([hostPoints, null]);
            if (this.hostTotal() >= this.target) {
              break;
            }
          }
          done();
        }
      },

      determineResult: function(withdraw) {
        var playerTotal = this.playerTotal();
        var hostTotal = this.hostTotal();
        if (playerTotal > this.target) {
          // if player busted, player lose
          this.player.logGame(false);
          this.result = 0;
        } else if (hostTotal > this.target) {
          this.player.logGame(true);
          this.result = 1;
        } else if (playerTotal === this.target && hostTotal === this.target) {
          this.result = -1; // tie
        } else if (playerTotal === this.target) {
          this.player.logGame(true);
          this.result = 1;
        } else if (hostTotal === this.target) {
          this.player.logGame(false);
          this.result = 0;
        } else if (withdraw) {
          if (playerTotal === hostTotal) {
            this.result = -1; // tie
          } else if (playerTotal > hostTotal) {
            this.player.logGame(true);
            this.result = 1;
          } else {
            this.player.logGame(false);
            this.result = 0;
          }
        } // else continue

        return this.result !== null;
      }
    };

    return Game;
  })
  .factory('GameFactory', function(GameEngine, Game, Player) {
    var host = new Player('computer');

    function GameFactory() {
      this.engine = new GameEngine(2, 6, 5);
      this.host = host;
    }

    GameFactory.prototype = {
      create: function(player) {
        return new Game(this.engine, this.host, player);
      }
    };

    return GameFactory;
  })
  .controller('GameCtrl', function($scope, $modal, GameFactory, Player) {
    var self = this;
    this.player = null;
    this.factory = new GameFactory();

    this.newGame = function() {
      self.game = self.factory.create(self.player);
      self.game.next(true);
      console.log('new game created', self.game);
    };

    this.hit = function() {
      this.game.next(true);
    };

    this.stay = function() {
      this.game.next(false);
    };

    this.chanceOfBusting = function() {
      if (!this.game || this.game.result !== null) {
        return null;
      }
      var chance = this.game.engine.chanceOfBusting(this.game.playerTotal(), this.game.target);
      return chance;
    };

    $modal.open({
      templateUrl: '/template/create-user.html',
      backdrop: 'static',
      controller: 'UserCtrl'
    }).result.then(function(name) {
      self.player = new Player(name);
      self.factory.create(self.player);
      $scope.$emit('game:message', {
        type: 'success',
        msg: 'Welcome, ' + name + '. Let\'s play some games'
      });
    });
  })
  .controller('UserCtrl', function($scope, $modalInstance) {
    $scope.ok = function(playerName) {
      if (playerName) {
        $modalInstance.close(playerName);
      }
    };
  });
