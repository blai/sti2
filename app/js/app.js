angular.module('sti2', ['ui.bootstrap'])
  .factory('roller', function () {
    Roller.init();
    Roller.d6 = 2;

    return Roller;
  })
  .controller('RollerCtrl', function (roller) {
    document.addEventListener('roller:results', function (e) {
      console.log('Results: ' + e.values.join(', '));
    });
    
    this.roll = function () {
      roller.roll();
    }
  })
  .run(function () {
    Physijs.scripts.worker = '/vendor/physijs/physijs_worker.js';
    Physijs.scripts.ammo = '/vendor/physijs/examples/js/ammo.js';
  });