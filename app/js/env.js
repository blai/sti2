angular.module('sti2')
  .controller('MessagesCtrl', function($rootScope, $interval) {
    var self = this;
    this.messages = [];

    this.closeMessage = function(index) {
      self.messages.splice(index, 1);
    };

    $rootScope.$on('game:message', function(event, msg) {
      self.messages.push(msg);
    });

    $interval(function() {
      self.messages.shift();
    }, 6000);
  })
  .filter('percentage', function($filter) {
    return function(input, decimals) {
      return $filter('number')(input * 100, decimals) + '%';
    };
  });
