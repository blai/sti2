var gulp = require('gulp');
var prefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var webserver = require('gulp-webserver');

gulp.task('css', function() {
  gulp.src('./app/css/main.raw.css')
    .pipe(prefix())
    .pipe(rename('main.css'))
    .pipe(gulp.dest('./app/css'));
});

gulp.task('watch', function() {
  gulp.src('app')
    .pipe(webserver({
      livereload: true,
      open: true
    }));
  gulp.watch('./app/css/main.raw.css', ['css']);
});

gulp.task('default', ['css']);
